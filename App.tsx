import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import UserLayout from './layout/UserLayout';
import PersonList from './views/person/PersonList';
import PersonDetails from './views/person/PersonDetails'

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="UserLayout">
        <Stack.Screen name="UserLayout" component={UserLayout}/>
        <Stack.Screen name="PersonList" component={PersonList}/>
        <Stack.Screen name="PersonDetails" component={PersonDetails}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
