import { StatusBar } from 'expo-status-bar';
import React, { useDebugValue } from 'react';
import { StyleSheet, Text, View, SafeAreaView, FlatList, Image, TouchableOpacity, InteractionManager } from 'react-native';
import { useNavigation } from '@react-navigation/native'



const Unidade = ({item}) => {
    const navigation = useNavigation();
    return(
        <View >
            <TouchableOpacity
            style={styles.container}
            onPress={() => {navigation.navigate('PersonDetails',{id: item.id});}}
            >
                <Image
                style={styles.avatar}
                source={{uri: item.image}}/>
                <Text>Teste, {item.name} </Text>
            </TouchableOpacity>
        </View>
    );
}

const UserListComponent = ({item}) => {
    return(
        <Unidade item={item}></Unidade>
    )
}

export default UserListComponent;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        padding: 5,
        backgroundColor: '#c0c0c0'
    },
    avatar: {
        width:40,
        height:40,
        marginRight:10
    }
});